$(function () {
            var dataTimeOptions = {
                sideBySide: true,
                format: 'DD-MM-YYYY HH:mm',
                allowInputToggle: true
            };
            
            $('#datetimepicker').datetimepicker(dataTimeOptions);
            
            $("#datetimepicker").on("dp.change", function (e) {
                var selectedValue = e.date.format(dataTimeOptions.format).toString();
                $('#selectedValue').html('<b>'+ selectedValue +'</b>');
                console.log('datetimepicker: ', selectedValue);
            });
        });