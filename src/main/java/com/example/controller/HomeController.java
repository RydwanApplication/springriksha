package com.example.controller;

import com.example.model.Order;
import com.example.model.Ride;
import com.example.model.User;
import com.example.repository.JpaOrderRepository;
import com.example.repository.JpaRideRepository;
import com.example.repository.JpaUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Map;

/**
 * Created by Huba on 30.08.2017.
 */
@Controller
public class HomeController {

    JpaUserRepository userRepository;

    @Autowired
    public HomeController(JpaUserRepository userRepository)
    {
        this.userRepository=userRepository;
    }


    @GetMapping(path = "/registration")
    public String registartion(User user) {
        return "registration";
    }

    @GetMapping("/403")
    public String error403(){
        return "/errors/403";
    }
}
