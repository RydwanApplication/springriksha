package com.example.controller;

import com.example.model.Order;
import com.example.model.Ride;
import com.example.model.User;
import com.example.repository.JpaOrderRepository;
import com.example.repository.JpaRideRepository;
import com.example.repository.JpaUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

/**
 * Created by Huba on 10.09.2017.
 */
@Controller
public class RepositoryController {

    JpaUserRepository userRepository;
    JpaRideRepository rideRepository;
    JpaOrderRepository orderRepository;

    @Autowired
    public RepositoryController(JpaUserRepository userRepository, JpaRideRepository rideRepository,
                                JpaOrderRepository orderRepository) {
        this.userRepository = userRepository;
        this.rideRepository = rideRepository;
        this.orderRepository = orderRepository;
    }

    @GetMapping(path = "/home")
    public ModelAndView offers(Order order) {
        ModelAndView modelAndView = new ModelAndView("rydwan");
        modelAndView.addObject("all_offers", rideRepository.findAll());

        return modelAndView;
    }


    @PostMapping("/registration")
    public String checkUserInfo(@Valid User user, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            System.out.println("Error in registration");
            return "registration";
        }

        userRepository.save(user);

        return "redirect:/home";
    }

    @PostMapping("/adding-drive")
    public String checkRideInfo(@Valid Ride ride, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            System.out.println("Error in adding drive");
            return "adding-drive";
        }
        rideRepository.save(ride);

        return "redirect:/home";
    }


    @PostMapping("/adding-order")
    public String checkRideInfo(@RequestParam("seats") Integer seats, @RequestParam("rideId") Long rideId) {
        Ride ride = rideRepository.findOne(rideId);
        Order order = new Order();

        if (ride.getSeatsLeft() >= seats) {
            order.setOrderSeatsNumber(seats);
            order.setOrderRideId(ride);
            ride.setSeatsLeft(ride.getSeatsLeft() - seats);
            orderRepository.save(order);
            rideRepository.save(ride);
        }
        return "redirect:/home";
    }


    @PostMapping("/searching")
    public ModelAndView searchByDestination(@RequestParam("fromPlace") String from, @RequestParam("toPlace") String to) {
        ModelAndView modelAndView = new ModelAndView("search-results");
        modelAndView.addObject("search_results", rideRepository.findByFromPlaceAndToPlaceIgnoreCase(from, to));
        return modelAndView;
    }
}