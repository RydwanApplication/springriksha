package com.example.model;

import javax.persistence.*;
import javax.validation.Constraint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name="order_table")
public class Order {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Long orderId;

	@ManyToOne
	@JoinColumn(name = "orderRideId")
	private Ride orderRideId;
	
	@ManyToOne
	@JoinColumn(name="orderPassangerId")
	private User orderPassangerId;

	@Column(name="orderSeatsNumber")
	private Integer orderSeatsNumber;

}
