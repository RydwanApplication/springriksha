package com.example.model;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;


@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name="user_table")
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Long userId;

	@NotBlank
	@Size()
	@Column
	String firstName;

	@NotBlank
	@Column
	String lastName;

	@NotBlank
	@Column
	String nick;

	@NotBlank
	@Column
	String password;

	@NotBlank
	@Column
	String email;

	@Column(name = "active")
	Boolean isActive;

	@Column
	String role;
	
	@OneToMany(mappedBy="rideDriver")
	List<Ride> userRides;
	
	@OneToMany(mappedBy="orderPassangerId")
	List<Order> userOrders;

	public User(String firstName, String lastName, String nick, String password, String email) {
			this.firstName = firstName;
		this.lastName = lastName;
		this.nick = nick;
		this.password = password;
		this.email = email;

	}
}
