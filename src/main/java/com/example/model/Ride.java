package com.example.model;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name="ride_table")
public class Ride {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Long rideId;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="rideDriver")
	private User rideDriver;

	@NotBlank
	@Column
	private String fromPlace;

	@NotBlank
	@Column
	private String toPlace;

	@NotBlank
	@Column
	private Date date;
	
	@OneToMany(mappedBy="orderRideId")
	private List<Order> rideOrders;


	@Column
	private Integer seatsLeft;

	@Column
	private String comments;

	@Column
	private Integer price;

	public Ride(String fromPlace, String toPlace, Date date, Integer seatsLeft, String comments, Integer price) {
		this.fromPlace = fromPlace;
		this.toPlace = toPlace;
		this.date = date;
		this.seatsLeft = seatsLeft;
		this.comments = comments;
		this.price = price;
	}
}
