package com.example;

import com.example.model.User;
import com.example.model.Ride;
import com.example.model.Order;
import com.example.repository.JpaOrderRepository;
import com.example.repository.JpaRideRepository;
import com.example.repository.JpaUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class RikshaApplication {


    JpaUserRepository userRepository;
    JpaRideRepository rideRepository;
    JpaOrderRepository orderRepository;

    @Autowired
    public RikshaApplication(JpaUserRepository userRepository, JpaRideRepository rideRepository,
                                JpaOrderRepository orderRepository)
    {
        this.userRepository=userRepository;
        this.rideRepository=rideRepository;
        this.orderRepository=orderRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(RikshaApplication.class, args);
    }


}