package com.example.repository;

import com.example.model.Ride;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

import static org.hibernate.hql.internal.antlr.HqlTokenTypes.FROM;

/**
 * Created by Huba on 07.09.2017.
 */

public interface JpaRideRepository extends CrudRepository<Ride,Long>{
    List<Ride> findByFromPlaceAndToPlaceIgnoreCase(String fromPlace, String toPlace);
}
