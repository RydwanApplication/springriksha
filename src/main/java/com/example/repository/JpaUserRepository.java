package com.example.repository;

import com.example.model.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Huba on 07.09.2017.
 */
public interface JpaUserRepository extends CrudRepository<User, Long> {
    User findOneByEmail(String login);
}
