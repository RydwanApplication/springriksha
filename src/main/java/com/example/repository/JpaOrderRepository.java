package com.example.repository;

import com.example.model.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by Huba on 07.09.2017.
 */
public interface JpaOrderRepository extends PagingAndSortingRepository<Order,Long> {
}
